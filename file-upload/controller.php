<?php
    //переменные и получения данных из формы
    $upload_dir = './files/';
    $upload_file = $upload_dir . basename($_FILES['file']['name']);
    $check = false;
    //проверка изображения на загружаник
    if ($_FILES['file']['size']==0) {
        echo '<p>Пожалуйста загрузите изображения</p>';
        $check= true;
    }
    else{
        //проверка изображения на розмер
        if ($_FILES['file']['size'] > 5000000) {
            echo '<p>Ваш файл слишком большой!</p>';
            $check = true;
        }
        //проверка изображения на формат
        if ($_FILES['file']['type'] != 'image/png' && $_FILES['file']['type'] != 'image/jpeg') {
            echo '<p>Неправельний формат изображения</p>';
            $check = true;
        }
    }
    //проверка $check = false
    if (!$check) {
        //отправка на сервер
        if (copy($_FILES['file']['tmp_name'], $upload_file))
            echo '<p>Файл был успешно загружен на сервер</p>';
    } else {
        echo '<p>Ошибка! Файл не может быть загружен на сервер!</p>';
        exit;
    }