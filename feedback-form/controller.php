<?php
    //переменные и получения данных из формы
    $from="mail@slavapleshkov.com";
    $name=htmlspecialchars(trim($_POST["name"]));
    $email=htmlspecialchars(trim($_POST["email"]));
    $subject=htmlspecialchars(trim($_POST["subject"]));
    $messages=htmlspecialchars(trim($_POST["messages"]));
    $headers="From: $from\r\n"."Reply-To: $from\r\n"."Content-type: text/plain; charset=utf-8\r\n";
    $check=false;
    //проверка на имя
    if (strlen($name)==0){
        $check=true;
        echo 'Пожалуйста, заполните поле «Имя»<br>';
    }
    //проверка на email
    if ($email=="" || !preg_match("/@/",$email)){
        $check=true;
        echo 'Пожалуйста, заполните поле «E-mail»<br>';
    }
    //проверка темы
    if (strlen($subject)==0){
        $check=true;
        echo 'Пожалуйста, заполните поле «Тема»<br>';
    }
    //проверка сообщение
    if (strlen($messages)==0){
        $check=true;
        echo 'Пожалуйста, заполните поле «Сообщения» <br>';
    }
    //проверка $check = false
    if (!$check) {
        //отправка на почту $from
        mail($email, $subject, $messages, $headers);
     echo 'Ваше сообщение было отправлено<br>';
    }
    echo '<a href="../">back</a>';
